package com.springboot.portal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Student {
	
	@Id
	private int studentId;
	private String studentName;
	private String english;
	private String physics;
	private String maths;
	private int semester;
	
	
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Student(int studentId, String studentName, String english, String physics, String maths, int semester) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.english = english;
		this.physics = physics;
		this.maths = maths;
		this.semester = semester;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getStudentId() {
		return studentId;
	}


	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}


	public String getStudentName() {
		return studentName;
	}


	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}


	public String getEnglish() {
		return english;
	}


	public void setEnglish(String english) {
		this.english = english;
	}


	public String getPhysics() {
		return physics;
	}


	public void setPhysics(String physics) {
		this.physics = physics;
	}


	public String getMaths() {
		return maths;
	}


	public void setMaths(String maths) {
		this.maths = maths;
	}


	public int getSemester() {
		return semester;
	}


	public void setSemester(int semester) {
		this.semester = semester;
	}


	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentName=" + studentName + ", english=" + english
				+ ", physics=" + physics + ", maths=" + maths + ", semester=" + semester + "]";
	}
	
	
	
}
