package com.springboot.portal.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

import com.springboot.portal.entity.Student;
import com.springboot.portal.entity.Users;

@Repository
public class UserRepository {

	public List<Users> getAllUsers() {
		List<Users> usersList = new ArrayList<Users>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/PORTAL", "sreenidhi", "password@1");
			System.setProperty("com.mysql.jdbc.Driver","");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * from USERS");
			
			while (rs.next()) {
//				int userId, String userName, String password, String role
				Users user = new Users(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
				usersList.add(user);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return usersList;
	}

	public Users getUserByName(String name) {
		Users user = new Users();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/PORTAL", "sreenidhi", "password@1");
			System.setProperty("com.mysql.jdbc.Driver","");
			PreparedStatement  pstmt = con.prepareStatement("SELECT * from USERS where userName = ?");
			pstmt.setString(1, name);
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
//				int userId, String userName, String password, String role
				 user = new Users(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
				
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return user;
	}

	public Student getBySemester(int semester) {
		Student student = new Student();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/PORTAL", "sreenidhi", "password@1");
			System.setProperty("com.mysql.jdbc.Driver","");
			PreparedStatement  pstmt = con.prepareStatement("SELECT * from Student where semester = ?");
			pstmt.setInt(1, semester);
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
//				int studentId, String studentName, String english, String physics, String maths, int semester
				student = new Student(rs.getInt(1), rs.getString(2), rs.getString(3),  rs.getString(4),  rs.getString(5), rs.getInt(6) );
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return student;
	}
	
	public List<Student> getStudentByName(String name) {
		List<Student> studentList = new ArrayList<Student>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/PORTAL", "sreenidhi", "password@1");
			System.setProperty("com.mysql.jdbc.Driver","");
			PreparedStatement  pstmt = con.prepareStatement("SELECT * from Student where studentName = ?");
			pstmt.setString(1, name);
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
//				int studentId, String studentName, String english, String physics, String maths, int semester
				Student student = new Student(rs.getInt(1), rs.getString(2), rs.getString(3),  rs.getString(4),  rs.getString(5), rs.getInt(6) );
				studentList.add(student);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return studentList;
	}
	
	
}
