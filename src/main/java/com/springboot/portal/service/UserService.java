package com.springboot.portal.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.portal.entity.Student;
import com.springboot.portal.entity.Users;
import com.springboot.portal.repository.UserRepository;

@Service
@Transactional
public class UserService {

	@Autowired
	UserRepository repository;
	
	public List<Users> getAllUsers() {
		return repository.getAllUsers();
	}
	
	public Users getUserByName(String name) {
		return repository.getUserByName(name);
	}
	
	public Student getBySemester(int semester) {
		return repository.getBySemester(semester);
	}
	
	public List<Student> getStudentByName(String name) {
		return repository.getStudentByName(name);
	}
}

