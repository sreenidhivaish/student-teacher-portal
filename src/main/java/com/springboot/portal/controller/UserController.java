package com.springboot.portal.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.springboot.portal.entity.Student;
import com.springboot.portal.entity.Users;
import com.springboot.portal.service.UserService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {
	@Autowired
	public UserService service;
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@GetMapping("/getAllUsers")
	public List<Users> getAllUsers() {
		System.out.println("inside getAllUsers");
	    return service.getAllUsers();
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@GetMapping("/getUserByName/{name}")
	public Users getUserByName(@PathVariable String name) {
	    try {
	    	Users user = service.getUserByName(name);
	    	System.out.println(user);
	        return user;
	    } catch (NoSuchElementException e) {
	        return new Users();
	    }      
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/getBySemester/{semester}")
	public Student getBySemester(@PathVariable int semester) {
	    try {
	    	Student student = service.getBySemester(semester);
	    	System.out.println(student);
	        return student;
	    } catch (NoSuchElementException e) {
	        return new Student();
	    }      
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/getStudentByName/{name}")
	public List<Student> getStudentByName(@PathVariable String name) {
	    try {
	    	List<Student> student = service.getStudentByName(name);
	    	System.out.println(student);
	        return student;
	    } catch (NoSuchElementException e) {
	        return new ArrayList<Student>();
	    }      
	}
	
	
}
